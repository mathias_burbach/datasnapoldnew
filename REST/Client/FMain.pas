unit FMain;

interface

uses
  System.Classes,
  Data.DB,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.StdCtrls,
  Vcl.Buttons,
  Vcl.DBCtrls,
  Vcl.ExtCtrls,
  Vcl.Grids,
  Vcl.DBGrids;

type
  TfrmMain = class(TForm)
    grdDepartment: TDBGrid;
    dscDepartment: TDataSource;
    pnlDepartment: TPanel;
    navDepartment: TDBNavigator;
    grdEmployee: TDBGrid;
    dscEmployee: TDataSource;
    pnlEmployee: TPanel;
    navEmployee: TDBNavigator;
    btnApplyUpdates: TBitBtn;
    procedure dscStateChange(Sender: TObject);
    procedure btnApplyUpdatesClick(Sender: TObject);
  private
    { Private declarations }
    procedure EnableApplyUpdatesButton;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses DMain;

procedure TfrmMain.btnApplyUpdatesClick(Sender: TObject);
begin
  dmoMain.ApplyUpdates;
  EnableApplyUpdatesButton;
end;

procedure TfrmMain.dscStateChange(Sender: TObject);
begin
  EnableApplyUpdatesButton;
end;

procedure TfrmMain.EnableApplyUpdatesButton;
begin
  btnApplyUpdates.Enabled := dmoMain.HasPendingChanges;
end;

end.

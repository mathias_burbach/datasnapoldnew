object dmoMain: TdmoMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 202
  Width = 408
  object conRESTServer: TDSRestConnection
    Port = 8080
    LoginPrompt = False
    Left = 48
    Top = 40
    UniqueId = '{F03CA66D-F923-49AA-B0E4-57F0B0F0B576}'
  end
  object mtbDepartment: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 48
    Top = 120
  end
  object mtbEmployee: TFDMemTable
    CachedUpdates = True
    IndexFieldNames = 'Dept_No'
    MasterSource = dscDepartment
    MasterFields = 'Dept_No'
    DetailFields = 'Dept_No'
    FetchOptions.AssignedValues = [evMode, evDetailCascade]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailCascade = True
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 280
    Top = 120
  end
  object dscDepartment: TDataSource
    DataSet = mtbDepartment
    Left = 168
    Top = 120
  end
end

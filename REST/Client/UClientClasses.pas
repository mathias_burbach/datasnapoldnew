//
// Created by the DataSnap proxy generator.
// 7/11/2015 6:47:08 PM
//

unit UClientClasses;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TsvmEmployeeClient = class(TDSAdminRestClient)
  private
    FGetDepartmentsWithEmployeesCommand: TDSRestCommand;
    FGetDepartmentsWithEmployeesCommand_Cache: TDSRestCommand;
    FApplyUpdatesCommand: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure GetDepartmentsWithEmployees(out Departments: TFDJSONDataSets; out Employees: TFDJSONDataSets; const ARequestFilter: string = '');
    procedure GetDepartmentsWithEmployees_Cache(out Departments: IDSRestCachedTFDJSONDataSets; out Employees: IDSRestCachedTFDJSONDataSets; const ARequestFilter: string = '');
    procedure ApplyUpdates(Deltas: TFDJSONDeltas);
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TsvmEmployee_GetDepartmentsWithEmployees: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Departments'; Direction: 2; DBXType: 37; TypeName: 'TFDJSONDataSets'),
    (Name: 'Employees'; Direction: 2; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TsvmEmployee_GetDepartmentsWithEmployees_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Departments'; Direction: 2; DBXType: 26; TypeName: 'String'),
    (Name: 'Employees'; Direction: 2; DBXType: 26; TypeName: 'String')
  );

  TsvmEmployee_ApplyUpdates: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: 'Deltas'; Direction: 1; DBXType: 37; TypeName: 'TFDJSONDeltas')
  );

implementation

procedure TsvmEmployeeClient.GetDepartmentsWithEmployees(out Departments: TFDJSONDataSets; out Employees: TFDJSONDataSets; const ARequestFilter: string);
begin
  if FGetDepartmentsWithEmployeesCommand = nil then
  begin
    FGetDepartmentsWithEmployeesCommand := FConnection.CreateCommand;
    FGetDepartmentsWithEmployeesCommand.RequestType := 'GET';
    FGetDepartmentsWithEmployeesCommand.Text := 'TsvmEmployee.GetDepartmentsWithEmployees';
    FGetDepartmentsWithEmployeesCommand.Prepare(TsvmEmployee_GetDepartmentsWithEmployees);
  end;
  FGetDepartmentsWithEmployeesCommand.Execute(ARequestFilter);
  if not FGetDepartmentsWithEmployeesCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetDepartmentsWithEmployeesCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Departments := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetDepartmentsWithEmployeesCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetDepartmentsWithEmployeesCommand.FreeOnExecute(Departments);
    finally
      FreeAndNil(FUnMarshal)
    end;
  end
  else
    Departments := nil;
  if not FGetDepartmentsWithEmployeesCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetDepartmentsWithEmployeesCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Employees := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetDepartmentsWithEmployeesCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetDepartmentsWithEmployeesCommand.FreeOnExecute(Employees);
    finally
      FreeAndNil(FUnMarshal)
    end;
  end
  else
    Employees := nil;
end;

procedure TsvmEmployeeClient.GetDepartmentsWithEmployees_Cache(out Departments: IDSRestCachedTFDJSONDataSets; out Employees: IDSRestCachedTFDJSONDataSets; const ARequestFilter: string);
begin
  if FGetDepartmentsWithEmployeesCommand_Cache = nil then
  begin
    FGetDepartmentsWithEmployeesCommand_Cache := FConnection.CreateCommand;
    FGetDepartmentsWithEmployeesCommand_Cache.RequestType := 'GET';
    FGetDepartmentsWithEmployeesCommand_Cache.Text := 'TsvmEmployee.GetDepartmentsWithEmployees';
    FGetDepartmentsWithEmployeesCommand_Cache.Prepare(TsvmEmployee_GetDepartmentsWithEmployees_Cache);
  end;
  FGetDepartmentsWithEmployeesCommand_Cache.ExecuteCache(ARequestFilter);
  Departments := TDSRestCachedTFDJSONDataSets.Create(FGetDepartmentsWithEmployeesCommand_Cache.Parameters[0].Value.GetString);
  Employees := TDSRestCachedTFDJSONDataSets.Create(FGetDepartmentsWithEmployeesCommand_Cache.Parameters[1].Value.GetString);
end;

procedure TsvmEmployeeClient.ApplyUpdates(Deltas: TFDJSONDeltas);
begin
  if FApplyUpdatesCommand = nil then
  begin
    FApplyUpdatesCommand := FConnection.CreateCommand;
    FApplyUpdatesCommand.RequestType := 'POST';
    FApplyUpdatesCommand.Text := 'TsvmEmployee."ApplyUpdates"';
    FApplyUpdatesCommand.Prepare(TsvmEmployee_ApplyUpdates);
  end;
  if not Assigned(Deltas) then
    FApplyUpdatesCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FApplyUpdatesCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FApplyUpdatesCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(Deltas), True);
      if FInstanceOwner then
        Deltas.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FApplyUpdatesCommand.Execute;
end;

constructor TsvmEmployeeClient.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TsvmEmployeeClient.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TsvmEmployeeClient.Destroy;
begin
  FGetDepartmentsWithEmployeesCommand.DisposeOf;
  FGetDepartmentsWithEmployeesCommand_Cache.DisposeOf;
  FApplyUpdatesCommand.DisposeOf;
  inherited;
end;

end.


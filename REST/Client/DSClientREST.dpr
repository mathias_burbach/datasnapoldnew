program DSClientREST;

uses
  Vcl.Forms,
  FMain in 'FMain.pas' {frmMain},
  DMain in 'DMain.pas' {dmoMain: TDataModule},
  UClientClasses in 'UClientClasses.pas',
  UCommon in '..\UCommon.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmoMain, dmoMain);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.

unit DMain;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.DSClientRest,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  FireDAC.Stan.StorageBin,
  IPPeerClient;

type
  TdmoMain = class(TDataModule)
    conRESTServer: TDSRestConnection;
    mtbDepartment: TFDMemTable;
    mtbEmployee: TFDMemTable;
    dscDepartment: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    procedure GetDepartmentsWithEmployees;
    procedure MarkChangesAsApplied;
  public
    { Public declarations }
    procedure ApplyUpdates;
    function HasPendingChanges: Boolean;
  end;

var
  dmoMain: TdmoMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
  Data.FireDACJSONReflect,
  UClientClasses,
  UCommon;

{$R *.dfm}

{ TdmoMain }

procedure TdmoMain.ApplyUpdates;
var
  Deltas: TFDJSONDeltas;
  svmEmployeeClient: TsvmEmployeeClient;
begin
  if mtbDepartment.State in dsEditModes then
    mtbDepartment.Post;
  if mtbEmployee.State in dsEditModes then
    mtbEmployee.Post;
  Deltas := TFDJSONDeltas.Create;
  try
    TFDJSONDeltasWriter.ListAdd(Deltas, cEmployees, mtbEmployee);
    TFDJSONDeltasWriter.ListAdd(Deltas, cDepartments, mtbDepartment);
    svmEmployeeClient := TsvmEmployeeClient.Create(conRESTServer);
    try
      svmEmployeeClient.ApplyUpdates(Deltas);
      MarkChangesAsApplied;
    finally
      svmEmployeeClient.Free;
    end;
  finally
//    Deltas.Free;
  end;
end;

procedure TdmoMain.DataModuleCreate(Sender: TObject);
begin
  GetDepartmentsWithEmployees;
end;

procedure TdmoMain.GetDepartmentsWithEmployees;
var
  svmEmployeeClient: TsvmEmployeeClient;
  Departments, Employees: TFDJSONDataSets;
begin
  svmEmployeeClient := TsvmEmployeeClient.Create(conRESTServer);
  try
    svmEmployeeClient.GetDepartmentsWithEmployees(Departments, Employees);
    Assert(TFDJSONDataSetsReader.GetListCount(Departments) = 1);
    mtbDepartment.AppendData(TFDJSONDataSetsReader.GetListValue(Departments, 0));
    Assert(TFDJSONDataSetsReader.GetListCount(Employees) = 1);
    mtbEmployee.AppendData(TFDJSONDataSetsReader.GetListValue(Employees, 0));
    MarkChangesAsApplied;
  finally
    svmEmployeeClient.Free;
  end;
end;

function TdmoMain.HasPendingChanges: Boolean;
begin
  Result := (mtbDepartment.ChangeCount > 0) or
            (mtbEmployee.ChangeCount > 0);
end;

procedure TdmoMain.MarkChangesAsApplied;
begin
  mtbDepartment.MergeChangeLog;
  mtbEmployee.MergeChangeLog;
end;

end.

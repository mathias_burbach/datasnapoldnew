unit DServerContainer;

interface

uses System.SysUtils, System.Classes,
  Datasnap.DSServer, Datasnap.DSCommonServer,
  Datasnap.DSAuth;

type
  TsrvContainer = class(TDataModule)
    dsnServer: TDSServer;
    sclEmployee: TDSServerClass;
    procedure sclEmployeeGetClass(DSServerClass: TDSServerClass;
                                  var PersistentClass: TPersistentClass);
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

function DSServer: TDSServer;

implementation


{$R *.dfm}

uses
  SEmployee;

var
  FModule: TComponent;
  FDSServer: TDSServer;

function DSServer: TDSServer;
begin
  Result := FDSServer;
end;

constructor TsrvContainer.Create(AOwner: TComponent);
begin
  inherited;
  FDSServer := dsnServer;
end;

destructor TsrvContainer.Destroy;
begin
  inherited;
  FDSServer := nil;
end;

procedure TsrvContainer.sclEmployeeGetClass(DSServerClass: TDSServerClass;
                                            var PersistentClass: TPersistentClass);
begin
  PersistentClass := TsvmEmployee;
end;


initialization
  FModule := TsrvContainer.Create(nil);
finalization
  FModule.Free;
end.


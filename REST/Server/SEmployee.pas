unit SEmployee;

interface

uses
  System.Classes,
  System.JSON,
  Data.DB,
  Data.FireDACJSONReflect,
  Datasnap.DSServer,
  Datasnap.DSAuth,
  DataSnap.DSProviderDataModuleAdapter,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.IB,
  FireDAC.Phys.IBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  FireDAC.Stan.StorageJSON,
  FireDAC.Stan.StorageBin;

type
{$METHODINFO ON}
  TsvmEmployee = class(TDataModule)
    conIB: TFDConnection;
    qryDepartment: TFDQuery;
    qryDepartmentDEPT_NO: TStringField;
    qryDepartmentDEPARTMENT: TStringField;
    qryDepartmentHEAD_DEPT: TStringField;
    qryDepartmentLOCATION: TStringField;
    qryDepartmentBUDGET: TBCDField;
    qryEmployee: TFDQuery;
    qryEmployeeEMP_NO: TSmallintField;
    qryEmployeeDEPT_NO: TStringField;
    qryEmployeeLAST_NAME: TStringField;
    qryEmployeeFIRST_NAME: TStringField;
    qryEmployeeSALARY: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure GetDepartmentsWithEmployees(out Departments, Employees: TFDJSONDataSets);
    procedure ApplyUpdates(const Deltas: TFDJSONDeltas);
  end;
{$METHODINFO OFF}

implementation


{$R *.dfm}


uses
  System.SysUtils,
  UCommon;

{ TsvmEmployee }

procedure TsvmEmployee.ApplyUpdates(const Deltas: TFDJSONDeltas);
var
  jsnApply: IFDJSONDeltasApplyUpdates;
begin
  // Create the apply object
  jsnApply := TFDJSONDeltasApplyUpdates.Create(Deltas);
  // Apply the department delta
  jsnApply.ApplyUpdates(cDepartments, qryDepartment.Command);
  if jsnApply.Errors.Count = 0 then
    // If no errors, apply the employee delta
    jsnApply.ApplyUpdates(cEmployees, qryEmployee.Command);
  if jsnApply.Errors.Count > 0 then
    // Raise an exception if any errors.
    raise Exception.Create(jsnApply.Errors.Strings.Text);
end;

procedure TsvmEmployee.GetDepartmentsWithEmployees(out Departments, Employees: TFDJSONDataSets);
begin
  Departments := TFDJSONDataSets.Create;
  TFDJSONDataSetsWriter.ListAdd(Departments, qryDepartment);
  Employees := TFDJSONDataSets.Create;
  TFDJSONDataSetsWriter.ListAdd(Employees, qryEmployee);
end;

end.


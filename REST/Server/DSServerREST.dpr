program DSServerREST;
{$APPTYPE GUI}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  FMain in 'FMain.pas' {frmMain},
  SEmployee in 'SEmployee.pas' {svmEmployee: TDataModule},
  DServerContainer in 'DServerContainer.pas' {srvContainer: TDataModule},
  UWebModule in 'UWebModule.pas' {WebModule2: TWebModule},
  UCommon in '..\UCommon.pas';

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.

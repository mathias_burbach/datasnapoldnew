program DSClientFDAC;

uses
  Vcl.Forms,
  FMain in 'FMain.pas' {frmMain},
  DMain in 'DMain.pas' {dmoMain: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmoMain, dmoMain);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.

unit DMain;

interface

uses
  System.Classes,
  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.DS,
  FireDAC.Phys.DSDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Phys.TDBXBase,
  FireDAC.Comp.Client,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Stan.Param,
  FireDAC.Comp.DataSet,
  FireDAC.Stan.StorageBin,
  IPPeerClient;

type
  TdmoMain = class(TDataModule)
    conDSServer: TFDConnection;
    FDSchemaAdapter: TFDSchemaAdapter;
    mtbDepartment: TFDMemTable;
    mtbEmployee: TFDMemTable;
    stpGetDepartmentsWithEmployees: TFDStoredProc;
    tbaDepartment: TFDTableAdapter;
    tbaEmployee: TFDTableAdapter;
    dscDepartment: TDataSource;
    stpApplyUpdates: TFDStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    procedure GetDepartmentsWithEmployees;
    procedure MarkChangesAsApplied;
  public
    { Public declarations }
    procedure ApplyUpdates;
    function HasPendingChanges: Boolean;
  end;

var
  dmoMain: TdmoMain;

implementation

uses
  System.SysUtils;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmoMain }

procedure TdmoMain.ApplyUpdates;
var
  memStream: TMemoryStream;
  i: integer;
  dst: TDataSet;
begin
  for i := 0 to FDSchemaAdapter.Count - 1 do
  begin
    dst := FDSchemaAdapter.DataSets[i];
    if dst <> nil then
      if dst.State in dsEditModes then
        dst.Post;
  end;
  memStream := TMemoryStream.Create;
  try
    FDSchemaAdapter.ResourceOptions.StoreItems := [siDelta, siMeta];
    FDSchemaAdapter.SaveToStream(memStream, TFDStorageFormat.sfBinary);
    memStream.Position := 0;
    stpApplyUpdates.Params[0].AsStream:= memStream;
    stpApplyUpdates.ExecProc;
    MarkChangesAsApplied;
  except
    On E: Exception do
      raise Exception.Create(E.Message);
  end;
end;

procedure TdmoMain.DataModuleCreate(Sender: TObject);
begin
  GetDepartmentsWithEmployees;
end;

procedure TdmoMain.GetDepartmentsWithEmployees;
var
  strStream: TStringStream;
begin
  stpGetDepartmentsWithEmployees.ExecProc;
  strStream := TStringStream.Create(stpGetDepartmentsWithEmployees.Params[0].AsBlob);
  try
    if strStream <> nil then
    begin
      strStream.Position := 0;
      FDSchemaAdapter.LoadFromStream(strStream, TFDStorageFormat.sfBinary);
    end;
  finally
    strStream.Free;
  end;
end;

function TdmoMain.HasPendingChanges: Boolean;
begin
  Result := (mtbDepartment.ChangeCount > 0) or
            (mtbEmployee.ChangeCount > 0);
end;

procedure TdmoMain.MarkChangesAsApplied;
begin
  mtbDepartment.MergeChangeLog;
  mtbEmployee.MergeChangeLog;
end;

end.

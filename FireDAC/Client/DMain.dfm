object dmoMain: TdmoMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 567
  object conDSServer: TFDConnection
    Params.Strings = (
      'DriverID=DS'
      'Port=211')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    ConnectedStoredUsage = [auDesignTime]
    Connected = True
    LoginPrompt = False
    Left = 48
    Top = 56
  end
  object FDSchemaAdapter: TFDSchemaAdapter
    Left = 160
    Top = 56
  end
  object mtbDepartment: TFDMemTable
    CachedUpdates = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Adapter = tbaDepartment
    Left = 48
    Top = 192
  end
  object mtbEmployee: TFDMemTable
    CachedUpdates = True
    IndexFieldNames = 'Dept_No'
    MasterSource = dscDepartment
    MasterFields = 'Dept_No'
    DetailFields = 'Dept_No'
    FetchOptions.AssignedValues = [evMode, evDetailCascade]
    FetchOptions.Mode = fmAll
    FetchOptions.DetailCascade = True
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    Adapter = tbaEmployee
    Left = 336
    Top = 192
  end
  object stpGetDepartmentsWithEmployees: TFDStoredProc
    Connection = conDSServer
    StoredProcName = 'TsvmEmployee.GetDepartmentsWithEmployees'
    Left = 329
    Top = 53
    ParamData = <
      item
        Name = 'ReturnValue'
        DataType = ftBlob
        ParamType = ptResult
      end>
  end
  object tbaDepartment: TFDTableAdapter
    SchemaAdapter = FDSchemaAdapter
    DatSTableName = 'qryDepartment'
    Left = 48
    Top = 128
  end
  object tbaEmployee: TFDTableAdapter
    SchemaAdapter = FDSchemaAdapter
    DatSTableName = 'qryEmployee'
    Left = 168
    Top = 128
  end
  object dscDepartment: TDataSource
    DataSet = mtbDepartment
    Left = 184
    Top = 192
  end
  object stpApplyUpdates: TFDStoredProc
    Connection = conDSServer
    StoredProcName = 'TsvmEmployee.ApplyUpdates'
    Left = 328
    Top = 120
    ParamData = <
      item
        Name = 'Delta'
        DataType = ftStream
        ParamType = ptInput
      end>
  end
end

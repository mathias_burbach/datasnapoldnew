object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'DS Client FireDAC'
  ClientHeight = 503
  ClientWidth = 565
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grdDepartment: TDBGrid
    Left = 0
    Top = 0
    Width = 565
    Height = 201
    Align = alTop
    DataSource = dscDepartment
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object pnlDepartment: TPanel
    Left = 0
    Top = 201
    Width = 565
    Height = 41
    Align = alTop
    TabOrder = 1
    object navDepartment: TDBNavigator
      Left = 16
      Top = 9
      Width = 240
      Height = 25
      DataSource = dscDepartment
      TabOrder = 0
    end
  end
  object grdEmployee: TDBGrid
    Left = 0
    Top = 242
    Width = 565
    Height = 220
    Align = alClient
    DataSource = dscEmployee
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object pnlEmployee: TPanel
    Left = 0
    Top = 462
    Width = 565
    Height = 41
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      565
      41)
    object navEmployee: TDBNavigator
      Left = 16
      Top = 9
      Width = 225
      Height = 25
      DataSource = dscEmployee
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
      TabOrder = 0
    end
    object btnApplyUpdates: TBitBtn
      Left = 431
      Top = 9
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Apply Updates'
      Enabled = False
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
        00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
        00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
        00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
        0003737FFFFFFFFF7F7330099999999900333777777777777733}
      NumGlyphs = 2
      TabOrder = 1
      OnClick = btnApplyUpdatesClick
    end
  end
  object dscDepartment: TDataSource
    DataSet = dmoMain.mtbDepartment
    OnStateChange = dscStateChange
    Left = 72
    Top = 72
  end
  object dscEmployee: TDataSource
    DataSet = dmoMain.mtbEmployee
    OnStateChange = dscStateChange
    Left = 64
    Top = 296
  end
end

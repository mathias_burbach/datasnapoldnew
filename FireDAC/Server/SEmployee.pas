unit SEmployee;

interface

uses
  System.Classes,
  System.Generics.Collections,
  System.JSON,
  Data.DB,
  Datasnap.DSServer,
  DataSnap.DSProviderDataModuleAdapter,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.IB,
  FireDAC.Phys.IBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  FireDAC.Stan.StorageBin;

type
{$METHODINFO ON}
  TsvmEmployee = class(TDataModule)
    conIB: TFDConnection;
    qryDepartment: TFDQuery;
    qryDepartmentDEPT_NO: TStringField;
    qryDepartmentDEPARTMENT: TStringField;
    qryDepartmentHEAD_DEPT: TStringField;
    qryDepartmentLOCATION: TStringField;
    qryDepartmentBUDGET: TBCDField;
    FDSchemaAdapter: TFDSchemaAdapter;
    qryEmployee: TFDQuery;
    qryEmployeeEMP_NO: TSmallintField;
    qryEmployeeDEPT_NO: TStringField;
    qryEmployeeLAST_NAME: TStringField;
    qryEmployeeFIRST_NAME: TStringField;
    qryEmployeeSALARY: TBCDField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure FDSchemaAdapterUpdateRow(ASender: TObject; ARow: TFDDatSRow;
      ARequest: TFDUpdateRequest; AUpdRowOptions: TFDUpdateRowOptions;
      var AAction: TFDErrorAction);
    procedure FDSchemaAdapterBeforeApplyUpdate(Sender: TObject);
    procedure FDSchemaAdapterAfterApplyUpdate(Sender: TObject);
  private
    { Private declarations }
    FErrorsList: TList<TJSONObject>;
    function GenerateErrorMessage: string;
  public
    { Public declarations }
    function GetDepartmentsWithEmployees: TStream;
    procedure ApplyUpdates(Delta: TStream);
  end;
{$METHODINFO OFF}

implementation

uses
  System.SysUtils;

const
  cErrorsOnApplyUpdates = 'Unexpected errors applying updates:'#10#13'%s';

{$R *.dfm}


function CopyStream(const AStream: TStream): TMemoryStream;
var
  LBuffer: TBytes;
  LCount: Integer;
begin
  Result := TMemoryStream.Create;
  try
    SetLength(LBuffer, 1024 * 32);
    while True do
    begin
      LCount := AStream.Read(LBuffer, Length(LBuffer));
      Result.Write(LBuffer, LCount);
      if LCount < Length(LBuffer) then
        break;
    end;
  except
    Result.Free;
    raise;
  end;
end;

{ TsvmEmployee }

procedure TsvmEmployee.ApplyUpdates(Delta: TStream);
var
  memStream: TMemoryStream;
  iErrors: Integer;
begin
  iErrors := 0;
  memStream := CopyStream(Delta);
  memStream.Position := 0;
  try
    FDSchemaAdapter.LoadFromStream(memStream, TFDStorageFormat.sfBinary);
    iErrors := FDSchemaAdapter.ApplyUpdates
  finally
    memStream.Free;
    if iErrors > 0 then
      raise Exception.CreateFmt(cErrorsOnApplyUpdates , [GenerateErrorMessage]);
  end;
end;

procedure TsvmEmployee.DataModuleCreate(Sender: TObject);
begin
  FErrorsList := TList<TJSONObject>.create;
end;

procedure TsvmEmployee.DataModuleDestroy(Sender: TObject);
begin
  FErrorsList.Free;
end;

procedure TsvmEmployee.FDSchemaAdapterAfterApplyUpdate(Sender: TObject);
begin
  //Beep;
end;

procedure TsvmEmployee.FDSchemaAdapterBeforeApplyUpdate(Sender: TObject);
begin
  //Beep;
end;

procedure TsvmEmployee.FDSchemaAdapterUpdateRow(ASender: TObject;
  ARow: TFDDatSRow; ARequest: TFDUpdateRequest;
  AUpdRowOptions: TFDUpdateRowOptions; var AAction: TFDErrorAction);
begin
  Beep; // not triggered
end;

function TsvmEmployee.GenerateErrorMessage: string;
var
  i: integer;
  jsnObject : TJSONObject;
  jsnArray: TJSONArray;
begin
  jsnObject := TJSONObject.Create;
  try
    jsnArray := TJSONArray.Create;
    try
      for i := FErrorsList.Count - 1 downto 0 do
        jsnArray.Add(FErrorsList[i]);
      jsnObject.AddPair('Errors', jsnArray);
      Result := jsnObject.ToJSON;
    except
      jsnArray.Free;
    end;
  except
    jsnObject.Free
  end;
  FErrorsList.Clear;
end;

function TsvmEmployee.GetDepartmentsWithEmployees: TStream;
begin
  Result := TMemoryStream.Create;
  qryDepartment.Open;
  qryEmployee.Open;
  FDSchemaAdapter.SaveToStream(Result, TFDStorageFormat.sfBinary);
  Result.Position := 0;
end;

end.


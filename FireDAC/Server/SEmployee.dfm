object svmEmployee: TsvmEmployee
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 182
  Width = 343
  object conIB: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Public\Documents\Embarcadero\Studio\17.0\Sampl' +
        'es\Data\Employee.gdb'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=IB')
    FetchOptions.AssignedValues = [evUnidirectional, evCursorKind]
    FetchOptions.CursorKind = ckForwardOnly
    ConnectedStoredUsage = [auDesignTime]
    Connected = True
    LoginPrompt = False
    Left = 40
    Top = 40
  end
  object qryDepartment: TFDQuery
    Active = True
    CachedUpdates = True
    Connection = conIB
    SchemaAdapter = FDSchemaAdapter
    SQL.Strings = (
      'Select Dept_No, Department, Head_Dept, Location, Budget'
      'From Department'
      'Order By Dept_No')
    Left = 128
    Top = 40
    object qryDepartmentDEPT_NO: TStringField
      FieldName = 'DEPT_NO'
      Origin = 'DEPT_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 3
    end
    object qryDepartmentDEPARTMENT: TStringField
      FieldName = 'DEPARTMENT'
      Origin = 'DEPARTMENT'
      Required = True
      Size = 25
    end
    object qryDepartmentHEAD_DEPT: TStringField
      FieldName = 'HEAD_DEPT'
      Origin = 'HEAD_DEPT'
      FixedChar = True
      Size = 3
    end
    object qryDepartmentLOCATION: TStringField
      FieldName = 'LOCATION'
      Origin = 'LOCATION'
      Size = 15
    end
    object qryDepartmentBUDGET: TBCDField
      FieldName = 'BUDGET'
      Origin = 'BUDGET'
      Precision = 18
      Size = 2
    end
  end
  object FDSchemaAdapter: TFDSchemaAdapter
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate, uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    BeforeApplyUpdate = FDSchemaAdapterBeforeApplyUpdate
    AfterApplyUpdate = FDSchemaAdapterAfterApplyUpdate
    OnUpdateRow = FDSchemaAdapterUpdateRow
    Left = 224
    Top = 40
  end
  object qryEmployee: TFDQuery
    Active = True
    CachedUpdates = True
    MasterFields = 'DEPT_NO'
    DetailFields = 'DEPT_NO'
    Connection = conIB
    SchemaAdapter = FDSchemaAdapter
    FetchOptions.AssignedValues = [evCache]
    SQL.Strings = (
      'Select Emp_No, Dept_No, Last_Name, First_Name, Salary'
      'From Employee'
      'Order By Last_Name')
    Left = 128
    Top = 104
    object qryEmployeeEMP_NO: TSmallintField
      FieldName = 'EMP_NO'
      Origin = 'EMP_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryEmployeeDEPT_NO: TStringField
      FieldName = 'DEPT_NO'
      Origin = 'DEPT_NO'
      Required = True
      FixedChar = True
      Size = 3
    end
    object qryEmployeeLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Origin = 'LAST_NAME'
      Required = True
    end
    object qryEmployeeFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
      Origin = 'FIRST_NAME'
      Required = True
      Size = 15
    end
    object qryEmployeeSALARY: TBCDField
      FieldName = 'SALARY'
      Origin = 'SALARY'
      Required = True
      Precision = 18
      Size = 2
    end
  end
end

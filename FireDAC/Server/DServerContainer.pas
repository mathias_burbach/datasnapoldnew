unit DServerContainer;

interface

uses
  System.Classes,
  Datasnap.DSTCPServerTransport,
  Datasnap.DSServer,
  Datasnap.DSCommonServer,
  IPPeerServer;

type
  TsvrContainer = class(TDataModule)
    dsnServer: TDSServer;
    tcpTransport: TDSTCPServerTransport;
    sclEmployee: TDSServerClass;
    procedure sclEmployeeGetClass(DSServerClass: TDSServerClass;
                                  var PersistentClass: TPersistentClass);
  private
    { Private declarations }
  public
  end;

var
  svrContainer: TsvrContainer;

implementation


{$R *.dfm}

uses
  SEmployee;

procedure TsvrContainer.sclEmployeeGetClass(DSServerClass: TDSServerClass;
                                            var PersistentClass: TPersistentClass);
begin
  PersistentClass := TsvmEmployee;
end;


end.


unit FMain;

interface

uses
  System.Classes,
  Data.DB,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.StdCtrls,
  Vcl.Buttons,
  Vcl.DBCtrls,
  Vcl.ExtCtrls,
  Vcl.Grids,
  Vcl.DBGrids;

type
  TfrmMain = class(TForm)
    grdDepartment: TDBGrid;
    pnlDepartment: TPanel;
    pnlEmployee: TPanel;
    grdEmployee: TDBGrid;
    dscDepartment: TDataSource;
    dscEmployee: TDataSource;
    navDepartment: TDBNavigator;
    navEmployee: TDBNavigator;
    btnApplyUpdates: TBitBtn;
    btnRefreshRecord: TBitBtn;
    procedure dscStateChange(Sender: TObject);
    procedure btnApplyUpdatesClick(Sender: TObject);
    procedure btnRefreshRecordClick(Sender: TObject);
  private
    { Private declarations }
    procedure EnableButtons;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  DMain;

procedure TfrmMain.btnApplyUpdatesClick(Sender: TObject);
begin
  dmoMain.ApplyUpdates;
  EnableButtons;
end;

procedure TfrmMain.btnRefreshRecordClick(Sender: TObject);
begin
  dmoMain.RefreshRecord;
end;

procedure TfrmMain.dscStateChange(Sender: TObject);
begin
  EnableButtons;
end;

procedure TfrmMain.EnableButtons;
begin
  btnApplyUpdates.Enabled := dmoMain.HasPendingChanges;
  btnRefreshRecord.Enabled := not dmoMain.HasPendingChanges;
end;

end.

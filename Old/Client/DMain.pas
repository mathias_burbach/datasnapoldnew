unit DMain;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Data.SqlExpr, Data.DBXDataSnap,
  IPPeerClient, Data.DBXCommon, Datasnap.DBClient, Datasnap.DSConnect;

type
  TdmoMain = class(TDataModule)
    conDSServer: TSQLConnection;
    prcEmployee: TDSProviderConnection;
    cdsDepartment: TClientDataSet;
    cdsDepartmentDEPT_NO: TStringField;
    cdsDepartmentDEPARTMENT: TStringField;
    cdsDepartmentHEAD_DEPT: TStringField;
    cdsDepartmentLOCATION: TStringField;
    cdsDepartmentBUDGET: TBCDField;
    cdsDepartmentqryEmployee: TDataSetField;
    cdsEmployee: TClientDataSet;
    cdsEmployeeEMP_NO: TSmallintField;
    cdsEmployeeDEPT_NO: TStringField;
    cdsEmployeeLAST_NAME: TStringField;
    cdsEmployeeFIRST_NAME: TStringField;
    cdsEmployeeSALARY: TBCDField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ApplyUpdates;
    function HasPendingChanges: Boolean;
    procedure RefreshRecord;
  end;

var
  dmoMain: TdmoMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmoMain }

procedure TdmoMain.ApplyUpdates;
begin
  cdsDepartment.ApplyUpdates(0);
end;

procedure TdmoMain.DataModuleCreate(Sender: TObject);
begin
  cdsDepartment.Open;
end;

function TdmoMain.HasPendingChanges: Boolean;
begin
  Result := (cdsDepartment.ChangeCount > 0);
end;

procedure TdmoMain.RefreshRecord;
begin
  cdsDepartment.RefreshRecord;
end;

end.

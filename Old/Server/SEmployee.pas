unit SEmployee;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.Provider,
  Datasnap.DSServer,
  DataSnap.DSProviderDataModuleAdapter,
  DataSnap.DBClient,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.IB,
  FireDAC.Phys.IBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TsvmEmployee = class(TDSServerModule)
    conIB: TFDConnection;
    qryDepartment: TFDQuery;
    qryDepartmentDEPT_NO: TStringField;
    qryDepartmentDEPARTMENT: TStringField;
    qryDepartmentHEAD_DEPT: TStringField;
    qryDepartmentLOCATION: TStringField;
    qryDepartmentBUDGET: TBCDField;
    dscDepartment: TDataSource;
    qryEmployee: TFDQuery;
    dspDeptEmployee: TDataSetProvider;
    qryEmployeeEMP_NO: TSmallintField;
    qryEmployeeDEPT_NO: TStringField;
    qryEmployeeLAST_NAME: TStringField;
    qryEmployeeFIRST_NAME: TStringField;
    qryEmployeeSALARY: TBCDField;
    procedure dspDeptEmployeeBeforeUpdateRecord(Sender: TObject;
                                                SourceDS: TDataSet;
                                                DeltaDS: TCustomClientDataSet;
                                                UpdateKind: TUpdateKind;
                                                var Applied: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses
  System.Variants;


{$R *.dfm}


procedure TsvmEmployee.dspDeptEmployeeBeforeUpdateRecord(Sender: TObject;
                                                         SourceDS: TDataSet;
                                                         DeltaDS: TCustomClientDataSet;
                                                         UpdateKind: TUpdateKind;
                                                         var Applied: Boolean);
var
  fld: TField;
begin
  // simulate business rule
  // normally setting a CreateStmp or UpdateStmp
  // here: increment salary by $1.00 each time
  if (SourceDS = qryEmployee) and (UpdateKind = ukModify) then
  begin
    fld := DeltaDS.FieldByName('Salary');
    if VarIsNull(fld.NewValue) Or VarIsEmpty(fld.NewValue) then
      fld.NewValue := fld.OldValue + 1
    else
      fld.NewValue := fld.NewValue + 1;
  end;

end;

end.


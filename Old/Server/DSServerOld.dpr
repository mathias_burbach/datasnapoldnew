program DSServerOld;

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  FMain in 'FMain.pas' {frmMain},
  SEmployee in 'SEmployee.pas' {svmEmployee: TDSServerModule},
  DServerContainer in 'DServerContainer.pas' {svrContainer: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TsvrContainer, svrContainer);
  Application.Run;
end.

